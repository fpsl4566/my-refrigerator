package com.iy.myrefrigeratorapi.model;

import com.iy.myrefrigeratorapi.enums.Advertisinompany;
import com.iy.myrefrigeratorapi.enums.PaymentType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class AdvertisingRequest {
    private Advertisinompany advertisinompany;
    private PaymentType paymentType;
    private Double paymentAmount;
    private LocalDate datePayment;
    private LocalDate dateContract;
    private Boolean isExpiration;
    private LocalDate dateExpiration;
    private String requested;
}
