package com.iy.myrefrigeratorapi.service;

import com.iy.myrefrigeratorapi.entity.Advertising;
import com.iy.myrefrigeratorapi.model.AdvertisingRequest;
import com.iy.myrefrigeratorapi.repository.AdvertisingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor

public class AdvertisingService {
    private final AdvertisingRepository advertisingRepository;

    public void setAdvertising (AdvertisingRequest request){
        Advertising addData =new Advertising();
        addData.setAdvertisinompany(request.getAdvertisinompany());
        addData.setPaymentType(request.getPaymentType());
        addData.setPaymentAmount(request.getPaymentAmount());
        addData.setDatePayment(request.getDatePayment());
        addData.setDateContract(request.getDateContract());
        addData.setIsExpiration(request.getIsExpiration());
        addData.setDateExpiration(request.getDateExpiration());
        addData.setRequested(request.getRequested());

        advertisingRepository.save(addData);
    }
}
