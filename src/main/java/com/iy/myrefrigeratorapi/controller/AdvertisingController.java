package com.iy.myrefrigeratorapi.controller;

import com.iy.myrefrigeratorapi.model.AdvertisingRequest;
import com.iy.myrefrigeratorapi.service.AdvertisingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/Advertising")
public class AdvertisingController {
    private final AdvertisingService advertisingService;

    @PostMapping("/Payment")
    public String setAdvertising(@RequestBody AdvertisingRequest request){
        advertisingService.setAdvertising(request);

        return "OK";
    }
}
