package com.iy.myrefrigeratorapi.repository;

import com.iy.myrefrigeratorapi.entity.Advertising;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdvertisingRepository extends JpaRepository  <Advertising,Long> {
}

